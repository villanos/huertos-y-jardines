<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'proyectopauladb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GxpnrP?l_epwg{IH<m_wx6WQTUJy6Y)7,3Ud&<|c:ptk9dofh3/}4LU.BRfF^cY&');
define('SECURE_AUTH_KEY',  '|OMu,[[(XPB<:#E3d/7qE1XLpe+9.pPk^9JF6SUo7:N*Vb~/HOx,K;RO&ecB!GTm');
define('LOGGED_IN_KEY',    'Ixr=ZAY)>xrwWk2Q_Gs0`YIHp<q`=Q{3oDdn>6L,Vyd~58-_aKvlxx]4I#n?6%uS');
define('NONCE_KEY',        'Ota4WW@gll8c=K>a4Xn6ToaF}BF.$znm%AK 6X6x| D%n@`:U.2/z 7a+3{/bpG?');
define('AUTH_SALT',        'GK7B /SXPXp=e$]$HN9q:JxM0FgoJW2vJ9]1 ZjK}&2On9t=dZ_wI%dI^0;5$0#`');
define('SECURE_AUTH_SALT', ')K^jyF*#XzU`vHl!JQ.C];3nL=niI*)T]6Vj>4&g!VT$jD0[%U%[#XY*WZU9/UZI');
define('LOGGED_IN_SALT',   'Rc}k5~I1HEW{/<aIE._BOt+- 3jb&:A}HE ~b1skF<uXRS.vrGr4IIz~h-2>&Nu#');
define('NONCE_SALT',       'e|!HgAyq0(Qgu#xG,B7[]nP?2`]`YN9*|v6ps!FgbE>JO,/-[hz^Wbp(0{3xKE.p');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
